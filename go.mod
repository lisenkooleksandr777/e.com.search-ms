module e/search

go 1.14

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/go-redis/redis v6.15.7+incompatible
	github.com/labstack/echo v3.3.10+incompatible
	github.com/labstack/gommon v0.3.0 // indirect
	github.com/valyala/fasttemplate v1.1.0 // indirect
	go.mongodb.org/mongo-driver v1.3.2
	golang.org/x/crypto v0.0.0-20200406173513-056763e48d71 // indirect
)
