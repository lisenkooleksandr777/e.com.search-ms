package main

import (
	"fmt"
	"net/http"

	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"

	"e/search/utils/config"

	"e/search/endpoints/event"
	"e/search/endpoints/search"
	"e/search/models/auth"
)

func main() {
	e := echo.New()

	// Middleware
	e.Use(middleware.Logger())
	e.Use(middleware.Recover())
	e.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowOrigins: []string{config.GetDataBaseURL()},
		AllowMethods: []string{http.MethodPost},
	}))

	jwtConfig := middleware.JWTConfig{
		Claims:     &auth.JwtUserClaims{},
		SigningKey: []byte(config.GetSecret()),
	}
	e.Use(middleware.JWTWithConfig(jwtConfig))

	// Routes
	//Event get event by id
	g := e.Group("/event")
	g.GET("/:id", event.Get)

	s := e.Group("/search")
	s.POST("/global", search.Globally)
	s.POST("/local", search.Locally)

	// Start server
	e.Logger.Fatal(e.Start(fmt.Sprintf(":%d", config.GetServerPort())))
}
