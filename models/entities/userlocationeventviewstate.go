package entities

import "time"

// UserLocalEventsViewState represent user data
type UserLocalEventsViewState struct {
	LastSeenDate     time.Time `json:"lastSeenDate"`
	CurretnStateDate time.Time `json:"curretnStateDate"`
}
