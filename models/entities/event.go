package entities

import (
	"time"

	primitive "go.mongodb.org/mongo-driver/bson/primitive"
)

// Event read event
type Event struct {
	ID          primitive.ObjectID `bson:"_id"  json:"id"`
	Name        string             `bson:"name"  json:"name"`
	Description string             `bson:"description"  json:"description"`
	LogoURL     string             `bson:"logoURL"  json:"logoURL"`
	Location    Location           `bson:"location"  json:"location"`
	DateStart   time.Time          `bson:"dateStart" json:"dateStart"`
	DateEnd     time.Time          `bson:"dateEnd" json:"dateEnd"`
	DateCreated time.Time          `bson:"dateCreated" json:"dateCreated"`
	OwnerID     string             `bson:"ownerID"  json:"ownerId"`
	Tags        []string           `bson:"tags"  json:"tags"`
}
