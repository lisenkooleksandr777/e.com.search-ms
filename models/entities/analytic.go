package entities

// Analytic an
type Analytic struct {
	Data                 interface{} `json:"data"`
	UsersLiked           int         `json:"usersLiked"`
	UsersVillPartisipate int         `json:"usersVillPartisipate"`
	UsersSeen            int         `json:"usersSeen"`
}
