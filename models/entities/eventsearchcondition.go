package entities

import (
	"time"
)

// EventSearchCondition search condition for event swiping
type EventSearchCondition struct {
	Location        Location  `json:"location"`
	Radius          int64     `json:"radius"`
	DateStartAfter  time.Time `json:"dateStartAfter"`
	DateStartBefore time.Time `json:"dateStartBefore"`
	Page            int64     `json:"page"`
	Tags            []string  `json:"tags"`
}

// EventLocalSearchCondition search condition for event swiping
type EventLocalSearchCondition struct {
	Location         Location  `json:"location"`
	DateStartAfter   time.Time `json:"dateStartAfter"`
	DateStartBefore  time.Time `json:"dateStartBefore"`
	AfterDateCreated time.Time `json:"afterDateCreated"`
}
