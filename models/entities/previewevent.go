package entities

import (
	"time"

	primitive "go.mongodb.org/mongo-driver/bson/primitive"
)

// PreviewEvent represent event dring searching
type PreviewEvent struct {
	ID        primitive.ObjectID `bson:"_id"  json:"id"`
	Name      string             `bson:"name"  json:"name"`
	LogoURL   string             `bson:"logoURL"  json:"logoURL"`
	Location  Location           `bson:"location"  json:"location"`
	DateStart time.Time          `bson:"dateStart" json:"dateStart"`
	DateEnd   time.Time          `bson:"dateEnd" json:"dateEnd"`
}
