package cache

import (
	"time"

	"github.com/go-redis/redis"

	"e/search/utils/config"
)

// Manager incapsulate functionality of cache:delete/insert/update entities
type Manager struct {
	Client *redis.Client
}

//NewCacheManager return new instance of CacheManager
func NewCacheManager(db int) *Manager {
	client := redis.NewClient(&redis.Options{
		Addr:     config.GetCacheConnectionSrtring(),
		Password: "", // no password set
		DB:       db, // use default DB
	})

	return &Manager{Client: client}
}

// Get record by key
func (cache *Manager) Get(key string) (string, error) {
	return cache.Client.Get(key).Result()
}

// Insert insert new value
func (cache *Manager) Insert(key string, value interface{}, duration time.Duration) (string, error) {
	return cache.Client.Set(key, value, duration).Result()
}
