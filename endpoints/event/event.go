package event

import (
	"errors"
	"net/http"

	"github.com/labstack/echo"

	"e/search/services/event"
)

//Get an event for event page
func Get(c echo.Context) error {
	id := c.Param("id")

	if id == "" {
		return c.JSON(http.StatusBadRequest, errors.New("Event id is missing"))
	}

	data, err := event.Get(id)

	if err != nil {
		return c.JSON(http.StatusBadRequest, errors.New("Invalid request"))
	}

	if data == nil {
		c.NoContent(http.StatusNotFound)
	}

	return c.JSON(http.StatusOK, data)
}
