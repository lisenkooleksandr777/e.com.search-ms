package search

import (
	"errors"
	"net/http"

	jwt "github.com/dgrijalva/jwt-go"
	"github.com/labstack/echo"

	"e/search/models/auth"
	"e/search/models/entities"
	"e/search/services/search"
)

//Globally search all over the world
func Globally(c echo.Context) error {
	user := c.Get("user").(*jwt.Token)
	if user == nil {
		return c.JSON(http.StatusUnauthorized, errors.New("Authentication data is not correct. "))
	}

	claims := user.Claims.(*auth.JwtUserClaims)
	if claims == nil {
		return c.JSON(http.StatusForbidden, errors.New("User claims are absent. "))
	}

	var searchCondition entities.EventSearchCondition
	err := c.Bind(&searchCondition)

	if err != nil {
		return c.JSON(http.StatusBadRequest, errors.New("Invalid request"))
	}

	data, err := search.Globally(&searchCondition)

	if err != nil {
		return c.JSON(http.StatusInternalServerError, errors.New("Oops, something went wrong"))
	}
	if data == nil {
		return c.NoContent(http.StatusNotFound)
	}
	return c.JSON(http.StatusOK, data)
}

//Locally search near to you, like Tinder
//Importent! Index on location.coordinates should be present
func Locally(c echo.Context) error {

	user := c.Get("user").(*jwt.Token)
	if user == nil {
		return c.JSON(http.StatusUnauthorized, errors.New("Authentication data is not correct. "))
	}

	claims := user.Claims.(*auth.JwtUserClaims)
	if claims == nil {
		return c.JSON(http.StatusForbidden, errors.New("User claims are absent. "))
	}

	var searchCondition entities.EventLocalSearchCondition
	err := c.Bind(&searchCondition)

	if searchCondition.Location.Coordinates == nil || len(searchCondition.Location.Coordinates) != 2 {
		return c.JSON(http.StatusBadRequest, errors.New("Location is absent. "))
	}

	if err != nil {
		return c.JSON(http.StatusBadRequest, errors.New("Invalid Request"))
	}

	data, err := search.Locally(&searchCondition, claims.IDentifier.Hex())

	if err != nil {
		return c.JSON(http.StatusInternalServerError, errors.New("Oops, something went wrong"))
	}
	if data == nil {
		return c.NoContent(http.StatusNotFound)
	}
	return c.JSON(http.StatusOK, data)
}
