package config

import (
	"encoding/json"
	"io/ioutil"
	"time"
)

//Configuration represent server configuration
type Configuration struct {
	DataBaseURL           string `json:"dataBaseUrl"`
	MongoTimeOut          int64  `json:"mongoTimeOut"`
	LoggerPath            string `json:"loggerPath"`
	ServerPort            int    `json:"serverPort"`
	ConnectionString      string `json:"connectionString"`
	CacheConnectionString string `json:"cacheConnectionString"`
	Secret                string `json:"secret"`
}

// config in
var config Configuration

func init() {
	dat, err := ioutil.ReadFile("config.json")
	if err == nil {
		err = json.Unmarshal(dat, &config)
		if err != nil {
			panic(err)
		}
	} else {
		panic(err)
	}
}

//GetSecret for genration of signature
func GetSecret() string {
	return config.Secret
}

// GetDataBaseURL get url of database
func GetDataBaseURL() string {
	return config.DataBaseURL
}

//GetMongoConnectionTimeOut get timeout for mongo connection
func GetMongoConnectionTimeOut() time.Duration {
	return time.Duration(config.MongoTimeOut * int64(time.Second))
}

// GetConnectionString get mondo conection string
func GetConnectionString() string {
	return config.ConnectionString
}

// GetServerPort get port that we use to host the app
func GetServerPort() int {
	return config.ServerPort
}

// GetCacheConnectionSrtring return connectino url to cache instance
func GetCacheConnectionSrtring() string {
	return config.CacheConnectionString
}
