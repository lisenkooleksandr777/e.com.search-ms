package search

import (
	"context"
	"log"
	"time"

	"e/search/models/entities"
	"e/search/utils/config"

	bson_ "go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

const (
	eventCollection = "events"
)

// Globally create and return new Event record
// index for geolocation needs to be add
func Globally(request *entities.EventSearchCondition, itemsPerPage int64) ([]entities.PreviewEvent, error) {

	mongoClient, err := createConnection()

	if err != nil {
		return nil, err
	}
	defer mongoClient.Disconnect(context.TODO())

	var eventCollection = mongoClient.Database("e_com").Collection(eventCollection)

	skip := request.Page * itemsPerPage
	cur, err := eventCollection.Find(context.Background(),
		buildEventGlobalSearchFilter(request),
		&options.FindOptions{Limit: &itemsPerPage, Skip: &skip, Sort: bson_.D{{"dateStart", 1}}})

	if err != nil {
		return nil, err
	}

	defer cur.Close(context.Background())

	var result []entities.PreviewEvent
	for cur.Next(context.Background()) {
		var currentItem entities.PreviewEvent

		err := cur.Decode(&currentItem)

		if err != nil {
			return nil, err
		}

		result = append(result, currentItem)
	}
	return result, nil
}

// Locally create and return new Event record
// index for geolocation needs to be added
func Locally(request *entities.EventLocalSearchCondition, itemsPerPage int64, radius int64) ([]entities.Event, error) {

	mongoClient, err := createConnection()

	if err != nil {
		return nil, err
	}
	defer mongoClient.Disconnect(context.TODO())

	var eventCollection = mongoClient.Database("e_com").Collection(eventCollection)

	cur, err := eventCollection.Find(context.Background(),
		buildEventLocalSearchFilter(request, radius),
		&options.FindOptions{Limit: &itemsPerPage, Sort: bson_.D{{"dateCreated", 1}}})

	if err != nil {
		return nil, err
	}

	defer cur.Close(context.Background())
	var result []entities.Event
	for cur.Next(context.Background()) {
		var currentItem entities.Event

		err := cur.Decode(&currentItem)

		if err != nil {
			return nil, err
		}

		result = append(result, currentItem)
	}
	return result, nil
}

/*    query builders  */
func buildEventGlobalSearchFilter(request *entities.EventSearchCondition) bson_.M {
	if request.Tags == nil || len(request.Tags) == 0 {
		if request.Location.Coordinates != nil && len(request.Location.Coordinates) == 2 {
			return bson_.M{
				"location.coordinates": buildLocationFilter(request.Location, request.Radius),
				"dateStart":            buildDateStartFilter(request.DateStartAfter, request.DateStartBefore)}
		}
		return bson_.M{
			"dateStart": buildDateStartFilter(request.DateStartAfter, request.DateStartBefore)}
	} else {
		if request.Location.Coordinates != nil && len(request.Location.Coordinates) == 2 {
			return bson_.M{
				"location.coordinates": buildLocationFilter(request.Location, request.Radius),
				"dateStart":            buildDateStartFilter(request.DateStartAfter, request.DateStartBefore),
				"tags":                 bson_.M{"$in": request.Tags},
			}
		}
		return bson_.M{
			"dateStart": buildDateStartFilter(request.DateStartAfter, request.DateStartBefore),
			"tags":      bson_.M{"$in": request.Tags},
		}
	}
}

/*    query builders  */
func buildEventLocalSearchFilter(request *entities.EventLocalSearchCondition, radius int64) bson_.M {
	return bson_.M{
		"location.coordinates": buildLocationFilter(request.Location, radius),
		"dateStart":            buildDateStartFilter(request.DateStartAfter, request.DateStartBefore),
		"dateCreated":          bson_.M{"$gt": request.AfterDateCreated},
	}
}

// see:  https://docs.mongodb.com/manual/reference/operator/query/nearSphere/#op._S_nearSphere
func buildLocationFilter(location entities.Location, radius int64) bson_.M {
	var gType = "Point"
	if location.Type != "" {
		gType = location.Type
	}
	return bson_.M{
		"$nearSphere": bson_.M{
			"$geometry": bson_.M{
				"type":        gType,
				"coordinates": location.Coordinates},
			"$minDistance": 0,
			"$maxDistance": radius * 1000,
		},
	}
}

func buildDateStartFilter(dateAfter time.Time, dateBefore time.Time) bson_.M {
	return bson_.M{
		"$gte": dateAfter,
		"$lt":  dateBefore,
	}
}

func createConnection() (*mongo.Client, error) {

	client, err := mongo.Connect(context.TODO(),
		options.Client().ApplyURI(config.GetConnectionString()).SetConnectTimeout(config.GetMongoConnectionTimeOut()))

	if err != nil {
		log.Fatal(err)
	}

	return client, err
}
