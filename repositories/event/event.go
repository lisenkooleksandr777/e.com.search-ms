package event

import (
	"context"
	"log"

	"e/search/models/entities"
	"e/search/utils/config"

	bson_ "go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

const (
	collectionEvents = "events"
)

//Get create and return new Event record
func Get(id string) (*entities.Event, error) {

	objectID, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		return nil, err
	}

	mongoClient, err := createConnection()

	if err != nil {
		return nil, err
	}
	defer mongoClient.Disconnect(context.TODO())

	var collection = mongoClient.Database("e_com").Collection(collectionEvents)
	res := collection.FindOne(context.Background(), bson_.D{{"_id", objectID}}, options.FindOne())

	var result entities.Event
	err = res.Decode(&result)
	if err != nil {
		return nil, err
	}
	return &result, nil
}

func createConnection() (*mongo.Client, error) {

	client, err := mongo.Connect(context.TODO(),
		options.Client().ApplyURI(config.GetConnectionString()).SetConnectTimeout(config.GetMongoConnectionTimeOut()))

	if err != nil {
		log.Fatal(err)
	}

	return client, err
}
