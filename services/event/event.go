package event

import (
	"e/search/models/entities"
	"e/search/repositories/event"
)

// Get return event entity for event page
func Get(id string) (*entities.Event, error) {
	return event.Get(id)
}
