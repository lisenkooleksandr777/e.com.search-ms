package search

import (
	"strconv"
	"time"

	"e/search/cache"
	"e/search/models/entities"
	"e/search/repositories/search"
)

const (
	viewTimeVindowState = "view_time_window_state"

	millisecondsInWeek = 604800000
)

// Locally find events near to user
func Locally(searchCondition *entities.EventLocalSearchCondition, userID string) ([]entities.Event, error) {
	if searchCondition.AfterDateCreated.IsZero() {
		key := viewTimeVindowState + userID
		cacheManager := cache.NewCacheManager(0)
		strDate, err := cacheManager.Get(key)
		if err == nil {
			stamp, err := strconv.ParseInt(strDate, 10, 64)
			if err == nil {
				searchCondition.AfterDateCreated = time.Unix(0, stamp)
			} else {
				searchCondition.AfterDateCreated = searchCondition.DateStartAfter
			}
		}
	}
	return search.Locally(searchCondition, 10, 50)
}

// Globally global search of events
func Globally(searchCondition *entities.EventSearchCondition) ([]entities.PreviewEvent, error) {
	return search.Globally(searchCondition, 10)
}
